/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{ts,tsx}"],
  theme: {
    screens: {
      sm: "600px",
      md: "768px",
      lg: "1168px",
      xl: "1379px",
    },
    extend: {
      colors: {
        cyan: {
          light: "#d6e5e6",
          DEFAULT: "#a1e2e6",
          dark: "#5c9699",
        },
        button: {
          light: "#79acaf",
          DEFAULT: "#5c9699",
        },
        beige: "#e6d88a",
        light: "#767676",
        link: {
          light: "#4a4a4a",
          DEFAULT: "#111111",
        },
      },
      fontFamily: {
        sans: [
          "-apple-system",
          "BlinkMacSystemFont",
          "Segoe UI",
          "Roboto",
          "Oxygen",
          "Ubuntu",
          "Cantarell",
          "Fira Sans",
          "Droid Sans",
          "Helvetica Neue",
          "sans-serif",
        ],
        serif: [
          "NonBreakingSpaceOverride",
          "Hoefler Text",
          "Baskerville Old Face",
          "Garamond",
          "Times New Roman",
          "serif",
        ],
        marker: [
          "Permanent Marker",
          "-apple-system",
          "BlinkMacSystemFont",
          "Segoe UI",
          "Roboto",
          "Oxygen",
          "Ubuntu",
          "Cantarell",
          "Fira Sans",
          "Droid Sans",
          "Helvetica Neue",
          "sans-serif",
        ],
      },
    },
  },
  plugins: [],
};
