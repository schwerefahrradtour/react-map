import { Link, LoaderFunctionArgs, useLocation } from "react-router-dom";
import { ReactComponent as ChevronLeftIcon } from "./assets/chevron_left.svg";
import { ReactComponent as PersonIcon } from "./assets/person.svg";
import { ReactComponent as TagIcon } from "./assets/tag.svg";
import { ReactComponent as WatchIcon } from "./assets/watch.svg";
import { ReactComponent as LocationIcon } from "./assets/location.svg";
import { Category } from "./model/category";
import { Post } from "./model/post";
import useSWR from "swr";
import { usePostId } from "./App";

export async function loader({
  params,
}: LoaderFunctionArgs): Promise<{ post: Post }> {
  const response = await fetch(
    `${process.env.REACT_APP_API_URL}/posts/${params.postId}`,
  );
  const post = await response.json();
  return { post };
}

export default function PostDetail() {
  const {
    state: locationState,
  }: {
    state: { post: Post; postCategories: Category[] } | null;
  } = useLocation();
  const postId = usePostId();
  const { data: postDetails } = useSWR<Post>(
    `${process.env.REACT_APP_API_URL}/posts/${postId}?_embed=author,wp:featuredmedia`,
  );
  const { data: postCategories } = useSWR<Category[]>(
    !locationState?.postCategories
      ? `${process.env.REACT_APP_API_URL}/categories?post=${postId}`
      : null,
  );

  const post = postDetails || locationState?.post;
  const categories = postCategories || locationState?.postCategories;
  const author = post?._embedded?.author?.[0];
  const featuredImage = post?._embedded?.["wp:featuredmedia"]?.[0];

  if (!post) {
    return <p>Loading post {postId}...</p>;
  }

  return (
    <div className="h-full bg-white">
      {featuredImage && (
        <a
          href={featuredImage.source_url}
          className="transition-opacity hover:opacity-70"
          target="_blank"
          rel="noreferrer"
        >
          <img
            src={featuredImage.media_details.sizes.medium_large?.source_url}
            alt={featuredImage.alt_text}
          />
        </a>
      )}

      <header className="flex items-center gap-2 pb-3 pe-5 ps-3 pt-5">
        <Link
          to="/"
          className="flex h-8 w-8 items-center justify-center hover:fill-cyan-dark"
        >
          <ChevronLeftIcon className="h-5 w-5" />
          <span className="sr-only ms-1">Back</span>
        </Link>
        <h2
          dangerouslySetInnerHTML={{
            __html: post.title.rendered ?? "No post provided.",
          }}
          className="font-marker text-sm md:text-base lg:text-lg"
        ></h2>
      </header>

      <div className="px-5 pb-5 text-xs lg:text-sm">
        <div className="mb-3 flex flex-wrap gap-x-3 gap-y-2">
          {author && (
            <span className="align-middle">
              <PersonIcon className="me-1 inline h-3 w-3 fill-light"></PersonIcon>
              <a href={author.link} className="text-light hover:underline">
                {author.name}
              </a>
            </span>
          )}
          <span className="align-middle">
            <WatchIcon className="me-1 inline h-3 w-3 fill-light"></WatchIcon>
            <span className="text-light">
              {new Date(post.date).toLocaleDateString(undefined, {
                dateStyle: "long",
              })}
            </span>
          </span>
          {categories ? (
            categories.length && (
              <span className="align-middle text-light">
                <TagIcon className="me-1 inline h-3 w-3 fill-light" />
                <span>
                  {categories.map((category, i) => (
                    <span key={category.id}>
                      <a href={category.link} className="hover:underline">
                        {category.name}
                      </a>
                      {i < categories.length - 1 && <>, </>}
                    </span>
                  ))}
                </span>
              </span>
            )
          ) : (
            <p>Loading categories...</p>
          )}
        </div>

        <div
          className="mb-3 font-serif text-sm lg:text-base"
          dangerouslySetInnerHTML={{ __html: post.excerpt.rendered }}
        ></div>

        <div className="flex flex-wrap gap-3">
          <a
            href={post.link}
            className="inline-flex rounded bg-button px-3 py-2 font-marker text-sm text-white transition-colors hover:bg-button-light"
          >
            Read More...
          </a>

          <Link
            className="group flex items-center font-marker text-sm transition-colors hover:text-light"
            to={`/posts/${post.id}`}
          >
            <LocationIcon className="me-1 h-4 w-4 transition-colors group-hover:fill-light" />
            Show on map
          </Link>
        </div>
      </div>
    </div>
  );
}
