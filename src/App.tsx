import classNames from "classnames";
import "leaflet/dist/leaflet.css";
import { useEffect, useRef, useState } from "react";
import { Outlet, useParams } from "react-router-dom";
import Map from "./Map";
import { Category } from "./model/category";
import { Post } from "./model/post";

export type LoaderData = {
  posts: Post[];
  categories: Category[];
};

export function usePostId(): number | null {
  const { postId } = useParams();
  return postId !== undefined ? Number.parseInt(postId) : null;
}

export async function loader(): Promise<LoaderData> {
  const [posts, categories] = await Promise.all([
    fetch(
      `${process.env.REACT_APP_API_URL}/posts?_embed=wp:featuredmedia`,
    ).then((res) => res.json()),
    fetch(`${process.env.REACT_APP_API_URL}/categories`).then((res) =>
      res.json(),
    ),
  ]);
  return {
    posts,
    categories,
  };
}

export default function App() {
  const postId = usePostId();
  const [selectedPostId, setSelectedPostId] = useState<number | null>(postId);
  const [hoverPostId, setHoverPostId] = useState<number | null>(null);
  const ref = useRef<HTMLDivElement | null>(null);
  const [height, setHeight] = useState("500px");

  useEffect(() => {
    function handleResize() {
      if (ref.current) {
        setHeight(`${window.innerHeight - ref.current.offsetTop}px`);
      }
    }

    handleResize();

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  });

  return (
    <div
      className="relative md:flex md:flex-row-reverse"
      style={{ height }}
      ref={ref}
    >
      <Map
        onMarkerClick={setSelectedPostId}
        hoverPostId={hoverPostId}
        onMarkerHover={setHoverPostId}
      />
      <div
        className={classNames({
          "absolute inset-x-0 z-[1000] overflow-y-auto md:static md:w-64 md:shrink-0 lg:w-1/3 lg:max-w-sm":
            true,
          "bottom-4": postId === null,
          "inset-y-0": postId !== null,
        })}
      >
        <Outlet
          context={{ selectedPostId, setSelectedPostId, setHoverPostId }}
        />
      </div>
    </div>
  );
}
