import { FeatureCollection } from "geojson";
import * as L from "leaflet";
import { useEffect, useState } from "react";
import {
  GeoJSON,
  MapContainer,
  Marker,
  TileLayer,
  useMap,
} from "react-leaflet";
import { useRouteLoaderData } from "react-router-dom";
import useSWRImmutable from "swr/immutable";
import { LoaderData, usePostId } from "./App";
import { Category } from "./model/category";
import { Post } from "./model/post";

export default function Map({
  onMarkerClick,
  hoverPostId,
  onMarkerHover,
}: {
  onMarkerClick: (postId: number | null) => void;
  hoverPostId: number | null;
  onMarkerHover: (postId: number | null) => void;
}) {
  const postId = usePostId();
  const { posts, categories } = useRouteLoaderData("root") as LoaderData;
  const [map, setMap] = useState<L.Map | null>(null);

  const post = posts?.find((post) => post.id === postId);
  const hoverPost = posts?.find((post) => post.id === hoverPostId);

  useEffect(() => {
    if (map) {
      setTimeout(() => map?.invalidateSize(), 100);
    }
  }, [map]);

  useEffect(() => {
    const position = post && getPostPosition(post);
    if (position) {
      map?.flyTo(position, 10);
    }
  }, [post, map]);

  return (
    <MapContainer
      center={[50, 35]}
      zoom={3}
      ref={setMap}
      className="h-full w-full"
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {post && <PostRoute post={post} flyToRoute={true} />}
      {hoverPost && hoverPost !== post && <PostRoute post={hoverPost} />}
      {posts?.map((post) => (
        <PostMarker
          key={post.id}
          post={post}
          postCategories={getPostCategories(post, categories)}
          onClick={onMarkerClick}
          onMouseOver={onMarkerHover}
        />
      ))}
    </MapContainer>
  );
}

function getPostCategories(post: Post, categories: Category[]): Category[] {
  return post.categories
    .map(
      (categoryId) =>
        categories?.find((category) => category.id === categoryId),
    )
    .filter((category) => !!category) as Category[];
}

function PostMarker({
  post,
  postCategories,
  onClick,
  onMouseOver,
}: {
  post: Post;
  postCategories: Category[];
  onClick: (postId: number | null) => void;
  onMouseOver: (postId: number | null) => void;
}) {
  const map = useMap();
  const position = getPostPosition(post);

  if (!position) {
    return <></>;
  }

  const firstCategorySlug = postCategories[0]?.slug || "uncategorized";

  return (
    <Marker
      position={position}
      icon={L.icon({
        iconUrl: `${
          process.env.REACT_APP_URL
        }/wp-content/themes/panda_siili_2019/images/map/${
          firstCategorySlug === "uncategorized" ? "banana" : firstCategorySlug
        }_pin.png`,
        iconSize: [64, 80], // size of the icon
        shadowSize: [64, 80], // size of the shadow
        iconAnchor: [32, 80], // point of the icon which will correspond to marker's location
        shadowAnchor: [32, 80], // the same for the shadow
        popupAnchor: [6, -80], // point from which the popup should open relative to the iconAnchor
      })}
      eventHandlers={{
        click: () => {
          onClick(post.id);
          map.flyTo(position, 10);
        },
        mouseover: () => onMouseOver(post.id),
        mouseout: () => onMouseOver(null),
      }}
    ></Marker>
  );
}

function getPostPosition(post: Post): L.LatLngExpression | null {
  const latStr = post.meta.latitude?.[0];
  const lngStr = post.meta.longitude?.[0];
  return latStr && lngStr
    ? [Number.parseFloat(latStr), Number.parseFloat(lngStr)]
    : null;
}

function PostRoute({
  post,
  flyToRoute = false,
}: {
  post: Post;
  flyToRoute?: boolean;
}) {
  const map = useMap();
  const { data, error } = useSWRImmutable<FeatureCollection, Error>(
    `${process.env.REACT_APP_URL}/wp-content/uploads/geojson/${post.slug}.geojson`,
  );

  useEffect(() => {
    if (!flyToRoute || !map || !data) {
      return;
    }
    const routeBounds = L.geoJSON(data).getBounds();
    if (routeBounds.isValid()) {
      map.flyToBounds(routeBounds);
    }
  }, [flyToRoute, map, data]);

  if (error) {
    console.error(error.message);
  }

  if (!data) {
    return <></>;
  }

  return <GeoJSON data={data} />;
}
