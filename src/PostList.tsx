import classNames from "classnames";
import { Fragment } from "react";
import { Link, useOutletContext, useRouteLoaderData } from "react-router-dom";
import { Category } from "./model/category";
import { LoaderData, usePostId } from "./App";

export default function PostList() {
  const postId = usePostId();
  const { posts, categories } = useRouteLoaderData("root") as LoaderData;
  const { selectedPostId, setSelectedPostId, setHoverPostId } =
    useOutletContext<{
      selectedPostId: number | null;
      setSelectedPostId: (postId: number | null) => void;
      setHoverPostId: (postId: number | null) => void;
    }>();

  if (posts.length === 0) {
    return <p>No posts yet.</p>;
  }

  return (
    <ul
      className={classNames({
        "mx-auto max-w-sm p-1 md:max-w-none": true,
        hidden: selectedPostId === null || postId !== null,
        "md:block": postId === null,
      })}
    >
      {posts.map((post) => {
        const postCategories = post.categories
          .map(
            (categoryId) =>
              categories?.find((category) => category.id === categoryId),
          )
          .filter((category) => !!category) as Category[];

        const featuredImage = post._embedded?.["wp:featuredmedia"]?.[0];

        return (
          <li
            key={post.id}
            className={classNames({
              "rounded md:mb-1 md:last:mb-0": true,
              "hover:bg-cyan-light": post.id !== postId,
              "block bg-beige": post.id === selectedPostId,
              "hidden bg-white md:block": post.id !== selectedPostId,
            })}
            onMouseOver={() => setHoverPostId(post.id)}
            onMouseOut={() => setHoverPostId(null)}
          >
            <Link
              to={`posts/${post.id}`}
              state={{ post, postCategories }}
              className="flex text-link hover:text-link"
              onClick={() => setSelectedPostId(post.id)}
            >
              {featuredImage && (
                <img
                  src={featuredImage.media_details.sizes.thumbnail.source_url}
                  alt={featuredImage.alt_text}
                  className="w-20 rounded-l object-cover"
                />
              )}
              <div className="px-4 py-3">
                <h3
                  className="font-marker text-sm"
                  dangerouslySetInnerHTML={{ __html: post.title.rendered }}
                ></h3>

                <p className="text-xs text-light">
                  {postCategories.map((category, i) => (
                    <Fragment key={category.id}>
                      <span>{category.name}</span>
                      {i < postCategories.length - 1 && <>, </>}
                    </Fragment>
                  ))}
                </p>
              </div>
            </Link>
          </li>
        );
      })}
    </ul>
  );
}
