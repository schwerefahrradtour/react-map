export type Entity = {
  id: number;
  slug: string;
  link: string;
};
