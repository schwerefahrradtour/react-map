import { Entity } from "./entity";

type Size =
  | "post-thumbnail"
  | "thumbnail"
  | "medium"
  | "medium_large"
  | "large"
  | "full";

export type Image = Entity & {
  source_url: string;
  alt_text: string;
  media_details: {
    sizes: {
      [key: string]: {
        source_url: string;
        width: number;
        height: number;
      };
    };
  };
};
