import { Entity } from "./entity";

export type Author = Entity & {
  name: string;
};
