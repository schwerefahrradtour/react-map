import { Author } from "./author";
import { Entity } from "./entity";
import { Image } from "./image";

export type Post = Entity & {
  title: RenderedBlock;
  meta: { latitude: string[]; longitude: string[] };
  date: string;
  categories: number[];
  excerpt: RenderedBlock;
  _embedded?: {
    author?: Author[];
    "wp:featuredmedia"?: Image[];
  };
};

export type RenderedBlock = {
  rendered: string;
};
