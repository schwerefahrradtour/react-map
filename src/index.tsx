import React from "react";
import ReactDOM from "react-dom/client";
import { RouterProvider, createHashRouter } from "react-router-dom";
import { SWRConfig } from "swr";
import App, { loader as appLoader } from "./App";
import PostDetail from "./PostDetail";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import PostList from "./PostList";

// const router = createBrowserRouter(
const router = createHashRouter(
  [
    {
      path: "/",
      element: <App />,
      loader: appLoader,
      id: "root",
      children: [
        {
          index: true,
          element: <PostList />,
        },
        {
          path: "posts/:postId",
          element: <PostDetail />,
          // loader: postDetailLoader,
        },
      ],
    },
  ],
  {
    // basename: process.env.REACT_APP_BASENAME,
  },
);

const fetcher = (resource: any, init: any) =>
  fetch(resource, init).then((res) => res.json());

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);
root.render(
  <React.StrictMode>
    <SWRConfig value={{ fetcher }}>
      <RouterProvider router={router} />
    </SWRConfig>
  </React.StrictMode>,
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
